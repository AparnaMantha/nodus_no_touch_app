/**
 * Created by Aparna on 6/27/2018.
 */
document.addEventListener('DOMContentLoaded', function () {

    document.getElementById("continueButton").addEventListener('click', continueFunction);
    document.getElementById("cloudProviderSaveButton").addEventListener('click', submitFunction);
});

function continueFunction() {
    var valid = true;

    if (!$('#first_name').val()) {
        if ($("#first_name").parent().next(".validation").length == 0) // only add if not added
        {
            $('#first_name').css('border', '1px solid red');
            $("#first_name").parent().after("<div class='validation ' style='color:red;padding: 2px;'> Required field</div>");

        }
        valid = false;
    } else {
        $("#first_name").parent().next(".validation").remove(); // remove it
        $('#first_name').css('border', '1px solid #000');
    }

    if (!$('#last_name').val()) {
        if ($("#last_name").parent().next(".validation").length == 0) // only add if not added
        {
            $('#last_name').css('border', '1px solid red');
            $("#last_name").parent().after("<div class='validation' style='color:red; padding: 2px;'> Required field</div>");

        }
        valid = false;
    } else {
        $("#last_name").parent().next(".validation").remove(); // remove it
        $('#last_name').css('border', '1px solid #000');
    }


    if (!$('#email').val()) {
        if ($("#email").parent().next(".validation").length == 0) // only add if not added
        {
            $("#email").parent().next(".validation1").remove(); // remove it
            $('#email').css('border', '1px solid red');
            $("#email").parent().after("<div class='validation' style='color:red; padding:2px;'> Required field</div>");
        }
        valid = false;
    } else {
        $("#email").parent().next(".validation").remove(); // remove it
        $("#email").parent().next(".validation1").remove(); // remove it
        var regemail = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        if (regemail.test($("#email").val()) == false) {
            $('#email').css('border', '1px solid red');
            $("#email").parent().after("<div class='validation1' style='color:red;padding:2px;'>Enter Valid Email </div>");
            valid = false;
        }
        else {
            $("#email").parent().next(".validation1").remove(); // remove it
            $('#email').css('border', '1px solid #000');
        }
    }

    if (!$('#business_phone').val()) {
        if ($("#business_phone").parent().next(".validation").length == 0) // only add if not added
        {
            $("#business_phone").parent().next(".validation1").remove(); // remove it
            $('#business_phone').css('border', '1px solid red');
            $("#business_phone").parent().after("<div class='validation' style='color:red;padding:2px;'>Required field</div>");
        }
        valid = false;
    } else {
        $("#business_phone").parent().next(".validation").remove(); // remove it
        $("#business_phone").parent().next(".validation1").remove(); // remove it
        var regbphone = /^\s*(?:\+?(\d{1,3}))?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*$/;    // /^\s*(?:\+?(\d{1,3}))?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*$/
        if(regbphone.test($("#business_phone").val())== false){
            $('#business_phone').css('border', '1px solid red');
            $("#business_phone").parent().after("<div class='validation1' style='color:red;padding:2px;'>Enter Valid Phone Number </div>");
            valid = false;
        }
        else {
            $("#business_phone").parent().next(".validation1").remove(); // remove it
            $('#business_phone').css('border', '1px solid #000');
        }
    }

    if (!$('#company').val()) {
        if ($("#company").parent().next(".validation").length == 0) // only add if not added
        {
            $('#company').css('border', '1px solid red');
            $("#company").parent().after("<div class='validation ' style='color:red;padding:2px;'> Required Field  </div>");
        }
        valid = false;
    } else {
        $("#company").parent().next(".validation").remove(); // remove it
        $('#company').css('border', '1px solid #000');
    }

    if (!$('#company_address1').val()) {
        if ($("#company_address1").parent().next(".validation").length == 0) // only add if not added
        {
            $('#company_address1').css('border', '1px solid red');
            $("#company_address1").parent().after("<div class='validation' style='color:red;padding:2px;'> Required Field  </div>");
        }
        valid = false;
    } else {
        $("#company_address1").parent().next(".validation").remove(); // remove it
        $('#company_address1').css('border', '1px solid #000');
    }

    if (!$('#city').val()) {
          if ($("#city").parent().next(".validation").length == 0) // only add if not added
        {
            $('#city').css('border', '1px solid red');
            error1.innerHTML = 'Required Field';
        }
        valid = false;
    } else {
        $("#city").css('border', '1px solid #000');
        error1.innerHTML = '';
    }

    if (!$('#state').val()) {
        if ($("#state").parent().next(".validation").length == 0) // only add if not added
        {
            $('#state').css('border', '1px solid red');
            error2.innerHTML = 'Required Field';
        }
        valid = false;
    } else {
        $("#state").css('border', '1px solid #000');
        error2.innerHTML = '';
     }

    if (!$('#zip_code').val()) {
        if ($("#zip_code").parent().next(".validation").length == 0) // only add if not added
        {
            $('#zip_code').css('border', '1px solid red');
            error3.innerHTML = 'Required Field';
        }
        valid = false;
    } else {
        $("#zip_code").css('border', '1px solid #000');
        error3.innerHTML = '';
    }

    if (!$('#country').val()) {
        if ($("#country").parent().next(".validation").length == 0) // only add if not added
        {
            $('#country').css('border', '1px solid red');
            $("#country").parent().after("<div class='validation' style='color:red;padding:2px;'>Required Field </div>");

        }
        valid = false;
    } else {
        $("#country").parent().next(".validation").remove(); // remove it
        $('#country').css('border', '1px solid gray');
    }

    if (!$('#password').val()) {
        if ($("#password").parent().next(".validation").length == 0) // only add if not added
        {
            $('#password').css('border', '1px solid red');
            $("#password").parent().after("<div class='validation' style='color:red;padding:2px;'>Required Field </div>");

        }
        valid = false;

    } else {
        $("#password").parent().next(".validation").remove(); // remove it
        $('#password').css('border', '1px solid #000');
    }


    if (!$('#confirm_password').val()) {
        if ($("#confirm_password").parent().next(".validation").length == 0) // only add if not added
        {
            $("#confirm_password").parent().next(".validation1").remove(); // remove it
            $('#confirm_password').css('border', '1px solid red');
            $("#confirm_password").parent().after("<div class='validation' style='color:red;padding:2px;'>Required Field </div>");
        }
        valid = false;
    } else {
        $("#confirm_password").parent().next(".validation").remove(); // remove it
        $("#confirm_password").parent().next(".validation1").remove();
        if ($("#password").val() != $('#confirm_password').val()) {
            $('#confirm_password').css('border', '1px solid red');
            $("#confirm_password").parent().after("<div class='validation1' style='color:red;padding:2px;'>Passwords do not match </div>");
            valid = false;
        }
        else {
            $("#confirm_password").parent().next(".validation1").remove(); // remove it
            $('#confirm_password').css('border', '1px solid #000');
        }
    }

    if (!$('#cloud_provider').val()) {
        if ($("#cloud_provider").parent().next(".validation").length == 0) // only add if not added
        {
            $('#cloud_provider').css('border', '1px solid red');
            $("#cloud_provider").parent().after("<div class='validation' style='color:red;padding:2px;'>Required Field </div>");
        }
        valid = false;
    } else {
        $("#cloud_provider").parent().next(".validation").remove(); // remove it
        $('#cloud_provider').css('border', '1px solid #000');
    }
    if (valid == true) {
        displayProviderForm();
    }
}

function displayProviderForm() {
    var x = document.getElementById("registeration_form");
    var y = document.getElementById("cloud_provider_form");
    if (x.style.display === "none") {
        x.style.display = "block";
        y.style.display = "none";
    } else {
        x.style.display = "none";
        y.style.display = " block";
    }
}

function submitFunction() {
    var status = true;

    if (!$('#aws_access_key').val()) {
        if ($("#aws_access_key").parent().next(".validation").length == 0) // only add if not added
        {
            $("#aws_access_key").parent().next(".validation1").remove(); // remove it
            $('#aws_access_key').css('border', '1px solid red');
            $("#aws_access_key").parent().after("<div class='validation' style='color:red;margin-left: 215px;text-align:left; padding: 2px;'> Required Field</div>");
        }
        status = false;
    } else {
        $("#aws_access_key").parent().next(".validation").remove(); // remove it
        $("#aws_access_key").parent().next(".validation1").remove(); // remove it
        var regaccesskey = /(^|[^A-Z0-9])[A-Z0-9]{20}(?![A-Z0-9])/;
        if(regaccesskey.test($("#aws_access_key").val())== false){
            $('#aws_access_key').css('border', '1px solid red');
            $("#aws_access_key").parent().after("<div class='validation1' style='color:red;padding:2px;text-align:left;margin-left: 215px;'>Enter Valid Access Key </div>");
            status = false;
        }
        else {
            $("#aws_access_key").parent().next(".validation1").remove(); // remove it
            $('#aws_access_key').css('border', '1px solid #000');
        }
    }

    if (!$('#aws_secret_key').val()) {
        if ($("#aws_secret_key").parent().next(".validation").length == 0) // only add if not added
        {
            $("#aws_secret_key").parent().next(".validation1").remove(); // remove it
            $('#aws_secret_key').css('border', '1px solid red');
            $("#aws_secret_key").parent().after("<div class='validation' style='color:red;margin-left: 215px;text-align:left; padding: 2px;'> Required Field</div>");
        }
        status = false;
    } else {
        $("#aws_secret_key").parent().next(".validation").remove(); // remove it
        $("#aws_secret_key").parent().next(".validation1").remove(); // remove it
        var regsecretkey = /(^|[^A-Za-z0-9/+=])[A-Za-z0-9/+=]{40}(?![A-Za-z0-9/+=])/;
        if(regsecretkey.test($("#aws_secret_key").val())== false){
            $('#aws_secret_key').css('border', '1px solid red');
            $("#aws_secret_key").parent().after("<div class='validation1' style='color:red;padding:2px;text-align:left;margin-left: 215px;'>Enter Valid Secret Key </div>");
            status = false;
        }
        else {
            $("#aws_secret_key").parent().next(".validation1").remove(); // remove it
            $('#aws_secret_key').css('border', '1px solid #000');
        }
    }

    if (!$('#region').val()) {
        if ($("#region").parent().next(".validation").length == 0) // only add if not added
        {
            $('#region').css('border', '1px solid red');
            $("#region").parent().after("<div class='validation' style='color:red; text-align:left;margin-left:215px; padding: 2px;'> Required Field</div>");
        }
        status = false;
    } else {
        $("#region").parent().next(".validation").remove(); // remove it
        $('#region').css('border', '1px solid #000');
    }
    if (status) {
        console.log("before form submit");
        document.getElementById("nodusform").submit();
      //  console.log("after form submit");
     //   window.location = "afterSubmit.html";
   }
}

function show(aval) {
    if (aval == "aws") {
        hidden_div_aws.style.display = 'inline-block';
    }
    else {
        hidden_div_aws.style.display = 'none';
    }
}

function selectionFunction() {
    var cloudprovider = document.getElementById("cloud_provider");
    var selectedvalue = cloudprovider.options[cloudprovider.selectedIndex].value;
    var cprovider = document.getElementById("cprovider");
    cprovider.value = selectedvalue;
    show(cprovider.options[cloudprovider.selectedIndex].value);
}
